const functions = require('firebase-functions');
const admin = require('firebase-admin');
const queue = require('firebase-queue');

admin.initializeApp(functions.config().firebase);


// const refPathVans = 'routeVansQueue/routeID/vans/{vanId}';
// exports.updateEta = functions.database
//   .ref(refPathVans)
//   .onWrite(event=>{
//     vanId = event.params.vanId;
//     if(event.data.val().startedFlag == null ) {
//         event.data.ref.parent.child(vanId+'/startedFlag').set(true);
        
//         var totalTime = parseTime(event.data.val().eta);
        
//         console.log(totalTime);
      
//         var time  = setInterval(
//           function(){
//                   var str = convertTimeToString(totalTime);
//                   if(event.data.val().eta!= "00:00"&&totalTime!=-1) {  
//                       event.data.ref.parent.child(vanId+'/eta').set(str);
//                       updateUsers(str,vanId);
//                     }else {
//                       cancelInterval(time);
//                     }
//                     totalTime--;
//             },1000);
//         }
// });


// function parseTime(time){
//   var arr = time.split(":");
//   var totalTime  = (arr[0]*60)+(arr[1]*1);
//   return totalTime;
// }


// function convertTimeToString(totalTime){
//   var str = (Math.floor(totalTime/60)).toLocaleString('en-US',
//         {minimumIntegerDigits:2,useGrouping:false})+
//         ':'+
//         (totalTime%60).toLocaleString('en-US',
//          {minimumIntegerDigits:2,useGrouping:false});
//   return str;
// }


// const refPathUsers = 'routeQueuers/routeID/queuers/';
// function updateUsers(str,plateNumber){
//   admin.database()
//   .ref(refPathUsers)
//   .once('value')
//   .then(snapshot=>{
//     snapshot.forEach(childSnapshot=>{
//     const userPlateNumber = childSnapshot.val().plateNumber;
//     const userId = childSnapshot.key;
//     console.log(userId,userPlateNumber);
//     if(plateNumber == userPlateNumber){
//         admin.database().ref(refPathUsers).child(userId+'/eta').set(str);
//       } 
//     })
//   });
// }


const refPathVans = 'routeVans/{routeID}/{plateNumber}';
exports.newVan = functions.database.ref(refPathVans).onWrite(event=>{
    var routeID = event.params.routeID;
    var plateNumber = event.params.plateNumber;
    var capacity = event.data.val().capacity;
    console.log(capacity);
    var generated = event.data.val().generated;
    console.log(generated);
    if(!generated){    
	    const refPathRouteQueue = 'routeQueue/' + routeID + '/totalSlots';
		admin.database().ref(refPathRouteQueue).once('value').then(snapshot=>{
			//Generate Slots
		    var currentSlots = snapshot.val();
		    console.log(currentSlots);
		    var totalSlots = capacity + currentSlots;
		    console.log(totalSlots);
		    for(var i = currentSlots;i<totalSlots+1;i++){
		      admin.database().ref('routeQueue/'+ routeID + '/slots/'+i+'/').set({'plateNumber':plateNumber});
		    }
		    admin.database().ref('routeQueue/'+routeID+'/totalSlots').set(totalSlots+1);
		    event.data.ref.child('generated').set(true);
		    //End Generate Slots

		    //Check for Unqueued User
		    var unQueuedUserRef = admin.database().ref("routeQueuers/" + routeID + "/queuers");
		    var unQueuedUsers = unQueuedUserRef.orderByChild("plateNumber").equalTo("N/A");
		    unQueuedUsers.once('value').then(function(snapshot){
		    	snapshot.forEach(function(user){
		    		var userID = user.key;
		    		user = user.val();
		    		user.userID = userID;
		    		user.plateNumber = null;
		    		console.log("Adding User to Slot " + user.number);
					var slotQueuerRef = admin.database().ref("routeQueue/" + routeID + "/slots/" + user.number);
					console.log(slotQueuerRef.toString());
					slotQueuerRef.once('value').then(function(snapshot){
						console.log(snapshot.val());
						if(snapshot.val() != null){
							console.log("User added to Slot");
							user.number = null;
							slotQueuerRef.child("queuer").set(user);
						} else {
							console.log("No van for user");
							admin.database().ref('routeQueuers/' + routeID + "/queuers/" + userID + "/plateNumber").set("N/A");
						}
					});
		    	});
		    });
		    //End Check
		});
	}
});

function generateSlots(capacity){
  
}

exports.newQueuers = functions.database.ref('routeQueuers/{routeID}/queuers/{userID}').onWrite(event=>{
	var routeID = event.params.routeID;
	var userID = event.params.userID;
	var userNumber = 0;
	var queuer = [];
	queuer.userID = userID;
	queuer.name = event.data.val().name;

	console.log("Function fired for user: " + userID);
	console.log(event.data.val());
	
	if(event.data.val().number == 0 && event.data.val().plateNumber != "N/A"){
		var countRef = admin.database().ref("routeQueuers/" + routeID + "/count");	
		console.log("Changing Count");
		countRef.transaction(function(count){
			if(count !== null){
				
				count++;
				userNumber = count;
				console.log("Count Updated");
			}
			return count;
		}, function(error, committed, snapshot){
			if(error){
				console.error(error);
			} else {				
				console.log("Adding Slot Number for Queuer");
				var userNumberRef = admin.database().ref("routeQueuers/" + routeID + "/queuers/" + userID + "/number");
				userNumberRef.set(userNumber);

				console.log("Adding User to Slot " + userNumber);
				var slotQueuerRef = admin.database().ref("routeQueue/" + routeID + "/slots/" + userNumber);
				console.log(slotQueuerRef.toString());
				slotQueuerRef.once('value').then(function(snapshot){
					console.log(snapshot.val());
					if(snapshot.val() != null){
						console.log("User added to Slot");
						slotQueuerRef.child("queuer").set(queuer);
					} else {
						console.log("No van for user");
						event.data.adminRef.child("plateNumber").set("N/A");
					}
				});
			}
		});
	}
	console.log("Function done");
});

exports.queuerVan = functions.database.ref('routeQueue/{routeID}/slots/{slotNumber}/queuer').onWrite(event=>{
	var routeID = event.params.routeID;
	var userID = event.data.val().userID;

	console.log("Assigning platenumber to user");
	var slotPlateNumberRef = event.data.adminRef.parent.child("plateNumber");
	var queuerPlateNumberRef = admin.database().ref('routeQueuers/' + routeID + '/queuers/' + userID + '/plateNumber');
	slotPlateNumberRef.once('value').then(function(snapshot){
		console.log("Platenumber assigned");
		queuerPlateNumberRef.set(snapshot.val());
	});

});

exports.newRoute = functions.database.ref('routes/{routeID}').onWrite(event=>{
	var routeID = event.params.routeID;

	var countRef = admin.database().ref("routeQueuers/" + routeID + "/count");
	countRef.once('value').then(function(snapshot){
		var count = snapshot.val();
		if(count == null){
			console.log("Adding Count for Route");
			countRef.set(0);
		}
	});
	
});
