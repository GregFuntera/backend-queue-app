const functions = require('firebase-functions');
const admin = require('firebase-admin');
const queue = require('firebase-queue');



var serviceAccount = require('./credentials.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://queue-app-f18fe.firebaseio.com/'
});

var queueMap = [];

var options = {
  'specId': 'queueUser'
};
var routesRef = admin.database().ref('routeQueuers');
routesRef.on('child_added', function(snapshot){
  var routeID = snapshot.key;
  var userQueueRefTasks = admin.database().ref("routeQueuers/" + routeID + "/notQueued");
  var userQueue = new queue(userQueueRefTasks, options, function(data, progress, resolve, reject){
    //Variables here
      var queueNumber;
      var currentVan;
      var plateNumber;
      var van;
      var remaining;
      var eta;
      var userID = data.userID;

    var countRef = admin.database().ref("routeQueuers/" + routeID + "/count")
      var currentVanRef = admin.database().ref("routeQueuers/"+routeID+"/currentVan");
      currentVanRef.once('value').then(function(snapshot){
        currentVan = snapshot.val();
        var queueNumberRef = admin.database().ref("routeVansQueue/" + routeID + "/queueNumber");
        queueNumberRef.once('value').then(function(snapshot){
            queueNumber = snapshot.val();
            if(queueNumber == currentVan){
              data.eta = "N/A";
              admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
              admin.database().ref("routeQueuers/"+routeID+"/noVan/"+userID).set(data);
            } else {
              var vanRef = admin.database().ref("routeVansQueue/"+routeID+"/vans/")
                  .orderByChild("queueNumber")
                  .equalTo(currentVan); 
              vanRef.once('value', function(snapshot){
                  snapshot.forEach(function(van){
                    plateNumber = van.key;
                    van = van.val();
                    remaining = van.remaining;
                    var vanRemainingRef = admin.database().ref("routeVansQueue/"+routeID+"/vans/"+plateNumber+"/remaining");
                    vanRemainingRef.transaction(function(remaining){
                        if(remaining !== null ){
                          remaining--;
                          if(remaining == 0){
                              currentVanRef.transaction(function(currentVan){
                                if(currentVan){
                                    currentVan++;
                                }
                              return currentVan;
                              })
                        }
                      }
                        return remaining;
                    });   
                    data.plateNumber = plateNumber;
                    admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
             
                  })
            });
          }
        }); 
    });
      setTimeout(function(){
        countRef.transaction(function(count){
            if(count !== null){
              count++;
              data.count = count;
            }
          return count;
        });
      admin.database().ref("routeQueuers/"+routeID+"/notQueued/tasks/"+userID).remove();
        resolve(data);
      }, 1000);
  }); 
  queueMap.routeID = userQueue;
});
  // var currentVanRef = admin.database().ref("routeQueuers/" + routeID + "/currentVan");
  // currentVanRef.once('value').then(function(snapshot){
  //   currentVan = snapshot.val();
  //   var queueNumberRef = admin.database().ref("routeVansQueue/" + routeID + "/queueNumber");
  //   queueNumberRef.once('value').then(function(snapshot){
  //     queueNumber = snapshot.val();
  //     if(queueNumber == currentVan){
  //       data.eta = eta;
  //       admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
  //       success = false;
  //     } else {
  //       var vanRef = admin.database().ref("routeVansQueue/"+routeID+"/vans/")
  //         .orderByChild("queueNumber")
  //         .equalTo(currentVan);
  //       vanRef.once('value', function(snapshot){
  //         snapshot.forEach(function(van){
  //           plateNumber = van.key;
  //           console.log("PlateNumber:" + plateNumber);

  //           van = van.val();

  //           remaining = van.remaining;
  //           console.log("Remaining:" + remaining);

  //           eta = van.eta;
  //           console.log("ETA:" + eta);

  //           var vanRemainingRef = admin.database().ref("routeVansQueue/"+routeID+"/vans/"+plateNumber+"/remaining");
  //           vanRemainingRef.transaction(function(remaining){
  //             if(remaining){
  //               remaining--;
  //               if(remaining == 0){
  //                 currentVanRef.transaction(function(currentVan){
  //                   if(currentVan){
  //                     currentVan++;
  //                   }
  //                   return currentVan;
  //                 })
  //               }
  //             }
  //             return remaining;
  //           });
            

  //           data.plateNumber = plateNumber;
  //           data.eta = eta;
  //           admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
  //           admin.database().ref("routeQueuers/"+routeID+"/notQueued/tasks/"+userID).remove();
  //         })
  //       });
  //     }
  //   }); 
  // });

  

// currentVanRef.transaction(function(currentVan){
//   if(currentVan){
//     currentVan++;
//   }
//   return currentVan;
// });






// var options = {
//   'specId': 'queueUser'
// };

// var userQueueRefTasks = admin.database().ref("notQueued");
// var userQueue = new queue(userQueueRefTasks, options, function(data, progress, resolve, reject){
//  var routeID = data.routeID;
//  var userID = data.userID;
//  var currentCount = 1;
//  var currentVan = 0;
//  var queueNumber = 0;
//  var remaining = 0;
//  var plateNumber = "";
//  var eta = "";

//  admin.database().ref("routeQueuers/" + routeID + "/count").once('value').then(function(snapshot){
//    //Get Current Count
//    currentCount = snapshot.val() + 1;
//    admin.database().ref("routeQueuers/" + routeID + "/currentVan").once('value').then(function(snapshot){
//      //Get Van currently being assigned to users
//      currentVan = snapshot.val();
//      admin.database().ref("routeVansQueue/" + routeID + "/queueNumber").once('value').then(function(snapshot){
//        //Get Next Queue Number of Vans
//        queueNumber = snapshot.val();
//        if(currentVan == queueNumber){
//          //No Van yet
//          data.eta = "N/A"
//          admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
//          admin.database().ref("routeQueuers/"+routeID+"/notQueued/"+userID).remove();
//          admin.database().ref("routeQueuers/"+routeID+"/count").set(currentCount);
//          reject(data);
//          userQueue.shutdown().then(function() {
//            console.log('Finished queue shutdown');
//          });
          
//        } else {
//          //Van is available (in venue or on the way)
//          var vanRef = admin.database().ref("routeVansQueue/"+routeID+"/vans/")
//            .orderByChild("queueNumber")
//            .equalTo(currentVan);
//          vanRef.once('value', function(snapshot){
//            snapshot.forEach(function(van){
//              plateNumber = van.key;
//              console.log("PlateNumber:" + plateNumber);

//              van = van.val();

//              remaining = van.remaining;
//              console.log("Remaining:" + remaining);

//              eta = van.eta;
//              console.log("ETA:" + eta);

//              admin.database().ref("routeVansQueue/"+routeID+"/vans/"+plateNumber+"/remaining").set(remaining-1)
//              if(remaining == 1){
//                admin.database().ref("routeQueuers/"+routeID+"/currentVan").set(currentVan+1);
//              }

//              data.plateNumber = plateNumber;
//              data.eta = eta;
//              admin.database().ref("routeQueuers/"+routeID+"/queuers/"+userID).set(data);
//              admin.database().ref("routeQueuers/"+routeID+"/notQueued/"+userID).remove();
//              admin.database().ref("routeQueuers/"+routeID+"/count").set(currentCount);
//              resolve(data);
//            })
//          });
//        }
//      });
//    });
//  });
// });