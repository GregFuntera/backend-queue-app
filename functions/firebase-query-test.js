const admin = require('firebase-admin');

var serviceAccount = require('./credentials.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://queue-app-f18fe.firebaseio.com/'
});

var queueRef = admin.database().ref("routeQueuers/routeID/queuers");


var i = 1;
var test = setInterval(function(){
	if(i == 50){
		clearInterval(test);
	}
	var userID = "user_" + i;
	var queuer= [];
	queuer.name = "Test " + i;
	queuer.code = "CODE_" + i++;
	queuer.number = 0;
	queueRef.child(userID).set(queuer);
}, 1000);
